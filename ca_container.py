from cellular_automaton import *
from ca_cooperation_antagonism import *

import PIL.Image as pi
import PIL.ImageDraw as pd
import numpy

import random

class CAContainer:
    def __init__(self, cellular_automaton: CellularAutomaton):
        self.cellular_automaton = cellular_automaton

    def run(self, steps, callback=None):
        for step in range(steps):
            self.cellular_automaton.evolve(1)
            if callback != None:
                callback(step, self.cellular_automaton)

    def cellsCounter(self, count_callback, count_return, normalize=False):
        """Count cells using count_callback function returning iterable of count_return (int) vars."""
        cells = self.cellular_automaton.get_cells()
        counts = [0 for _ in range(count_return)]
        for k in cells.keys():
            state = cells[k].state
            inc = count_callback(k, state)
            for i, x in enumerate(inc):
                counts[i] += x
        if not normalize:
            return counts
        else:
            return [c / (self.cellular_automaton.dimension[0] * self.cellular_automaton.dimension[1])
                    for c in counts]

    def exportImage(self, filepath, cell_color_function, mode="PNG", back_color=(0, 0, 0), scaleup=1):
        """Export image of current state to filepath, with cell_color_function (RGB)."""
        x = self.cellular_automaton.dimension[0]
        y = self.cellular_automaton.dimension[1]
        img = pi.new('RGB', (scaleup * x, scaleup * y), back_color)
        draw = pd.Draw(img)

        cells = self.cellular_automaton.get_cells()
        for k in cells.keys(): # k = tuple(2) = coordinates
            state = cells[k].state
            draw.rectangle([k[0]*scaleup, k[1]*scaleup, k[0]*scaleup+(scaleup-1), k[1]*scaleup+(scaleup-1)],
                            fill=cell_color_function(state))
            # TODO: use fromarray?

        img.save(filepath, mode)
        return img

    def exportConcentrationFieldsImage(self, filepath, mode="PNG", back_color=(0, 0, 0),
                                       tox_color=(255, 0, 0), coop_color=(0, 255, 0), scaleup=1,
                                       autoscale=True):
        """Export image of field concentrations for toxin/siderophore fields of CA."""
        x = self.cellular_automaton.dimension[0]
        y = self.cellular_automaton.dimension[1]
        img = pi.new('RGB', (scaleup * x, scaleup * y), back_color)
        draw = pd.Draw(img)

        if autoscale:
            max_tox = np.max(self.cellular_automaton.antagonating_concentration)
            max_coop = np.max(self.cellular_automaton.cooperating_concentration)
        else:
            max_tox = autoscale[0]
            max_coop = autoscale[1]
        for i in range(x):
            for j in range(y):
                ratio_t = self.cellular_automaton.antagonating_concentration[i, j] / max_tox
                ratio_c = self.cellular_automaton.cooperating_concentration[i, j] / max_coop
                col = (back_color[0] + int(ratio_t * tox_color[0]) + int(ratio_c * coop_color[0]),
                       back_color[1] + int(ratio_t * tox_color[1]) + int(ratio_c * coop_color[1]),
                       back_color[2] + int(ratio_t * tox_color[2]) + int(ratio_c * coop_color[2])) # ATTN: doesn't check for OOB
                       
                draw.rectangle([i*scaleup, j*scaleup, i*scaleup+(scaleup-1), j*scaleup+(scaleup-1)],
                               fill=col)
                # TODO: use fromarray?

        img.save(filepath, mode)
        return img

    def averageConcentrations(self):
        """Average field concentrations for toxin/siderophore at current timestep."""
        return [np.average(self.cellular_automaton.antagonating_concentration),
                np.average(self.cellular_automaton.cooperating_concentration)]

def countAlive(k, state):
    return [state[ALIVE]] # alive or dead

def countVarious(k, state):
    if state[ALIVE] >= 1: # alive
        return [1, state[ANTAGONIST], state[COOPERATOR], state[RESISTANCE],
                state[ANTAGONIST] > 0, state[COOPERATOR] > 0]
        # alive, antagonist, cooperator, resistance
    else:
        return [0, 0, 0, 0, False, False]

def state_to_color_int(*args):
    return tuple([int(x) for x in state_to_color(*args)])

def multiFunctions(inputs, *args):
    rets = []
    for func in args:
        ret = func(inputs)
        rets.append(ret)
    return rets

if __name__ == "__main__":
    autom = SimpleCooperationAndAntagonism("config.txt")

    # Minor initial param changes
    #autom.params['diffusion_length_toxin'] *= 100
    #autom.params['diffusion_length_siderophore'] *= 100
    param_change = 0.001
    for k in autom.params.keys():
        autom.params[k] += param_change * 2 * (0.5 - random.random()) * autom.params[k]
    
    cont = CAContainer(cellular_automaton=autom)
    cont.run(50, callback=lambda s, c : print("Step", s + 1))
    print(cont.cellsCounter(countVarious, 4, normalize=True))
    cont.exportImage("./images/test.png", state_to_color_int, scaleup=4)
    cont.exportConcentrationFieldsImage("./images/test_tc.png", scaleup=4)

    # Nutrition experiment
    autom = SimpleCooperationAndAntagonism("config.txt")
    nutrition_initial = autom.params['nutrition']
    toxin_effect_initial = autom.params['effect_toxin']
    cooperation_effect_initial = autom.params['effect_siderophore']

    NUM_STEPS = 11
    LOW_HIGH_PERCENT = 0.5 # percent of nutrition_initial
    GENERATIONS = 50

    for i in range(NUM_STEPS):
        print("Nutrition computation:", i + 1, "/", NUM_STEPS)
        nutrition = 0.5 * nutrition_initial + i * (nutrition_initial / (NUM_STEPS - 1))
        autom = SimpleCooperationAndAntagonism("config.txt") # reset
        autom.params['nutrition'] = nutrition
    
        cont = CAContainer(cellular_automaton=autom)
        cont.run(GENERATIONS, callback=lambda s, c : print("Step", s + 1))
        print(cont.cellsCounter(countVarious, 4, normalize=True))
        cont.exportImage("./images/nutri_" + str(i) + ".png", state_to_color_int, scaleup=4)
        cont.exportConcentrationFieldsImage("./images/nutri_tc_" + str(i) + ".png", scaleup=4)

    # Toxins experiment
    NUM_STEPS = 11
    LOW_HIGH_PERCENT = 0.5  # percent of nutrition_initial
    GENERATIONS = 50

    for i in range(NUM_STEPS):
        print("Toxin computation:", i + 1, "/", NUM_STEPS)
        teffect = 0.5 * toxin_effect_initial + i * (toxin_effect_initial / (NUM_STEPS - 1))
        autom = SimpleCooperationAndAntagonism("config.txt") # reset
        autom.params['effect_toxin'] = teffect
    
        cont = CAContainer(cellular_automaton=autom)
        cont.run(GENERATIONS, callback=lambda s, c : print("Step", s + 1))
        print(cont.cellsCounter(countVarious, 4, normalize=True))
        cont.exportImage("./images/toxin_" + str(i) + ".png", state_to_color_int, scaleup=4)
        cont.exportConcentrationFieldsImage("./images/toxin_tc_" + str(i) + ".png", scaleup=4)

    # Cooperation experiment
    NUM_STEPS = 11
    LOW_HIGH_PERCENT = 0.5 # percent of nutrition_initial
    GENERATIONS = 50

    for i in range(NUM_STEPS):
        print("Cooperator computation:", i + 1, "/", NUM_STEPS)
        ceffect = 0.5 * cooperation_effect_initial + i * (cooperation_effect_initial / (NUM_STEPS - 1))
        autom = SimpleCooperationAndAntagonism("config.txt") # reset
        autom.params['effect_siderophore'] = ceffect
    
        cont = CAContainer(cellular_automaton=autom)
        cont.run(GENERATIONS, callback=lambda s, c : print("Step", s + 1))
        print(cont.cellsCounter(countVarious, 4, normalize=True))
        cont.exportImage("./images/coop_" + str(i) + ".png", state_to_color_int, scaleup=4)
        cont.exportConcentrationFieldsImage("./images/coop_tc_" + str(i) + ".png", scaleup=4)
    
