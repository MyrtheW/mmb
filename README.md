# Cooperation and Antagonism in Microbial Communities
This is our final project for Multiscale Mathematical Biology 2020 \- 2021.

## Installation 
We use an adapted version of the [cellular automaton library](https://pypi.org/project/cellular-automaton/) as basis for our model. All files needed for the simulation can be downloaded from GitLab.

*pygame* and *Pillow*, which can be found on the *PyPI* repository, are also required.

## Usage
To run one simulation you can run `ca_cooperation_antagonism.py`, which will show the cell types over time. Parameters can be defined in the `config.txt` file. It currently contains the default parameter set that we used for our experiments. 
To see the effect of parameters, run `parameters_experiments.py`. First make a folder in the same directory, e.g. `images`, in which the images of the final states of the simulations will be saved.
You can define the parameters you want to change at the top of the document, a long with other experiment-specific variables: 

```python
PARAMS_TO_VARY = ['probability_mutation', 'probability_plasmid'] # list
NUM_STEPS = 11  # number of different parameter values for which you want to run the simulations. 
LOW_HIGH_PERCENT = 5  # ratio of initial parameter to reduce/increase by
HIGH_ONLY = True  # increase value only (to HIGH percent)
GENERATIONS = 150  # number of iterations the simulations are run. 
OUTPUT_PREFIX = "./images"  # make a folder with the name you define here.
CONFIG_FILE = "./config.txt"  # define the configureation file you want to use. 
DIMENSION = (200, 200)  # dimension of the simulation 
RANDOM_SEED = "random"  # "random" for random
TRACK_PER_ITERATION = True  
```

