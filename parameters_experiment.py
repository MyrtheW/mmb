from cellular_automaton import *
from ca_cooperation_antagonism import *
from ca_container import *

import random
import matplotlib.pyplot as plt
from matplotlib import cm

PARAMS_TO_VARY = ['probability_mutation', 'probability_plasmid'
                  , 'probability_disfunction'] # list
NUM_STEPS = 11
LOW_HIGH_PERCENT = 5 # ratio of initial parameter to reduce/increase by
HIGH_ONLY = True # increase value only (to HIGH percent)
GENERATIONS = 150
OUTPUT_PREFIX = "./images/mutnut2_"
CONFIG_FILE = "./config.txt"
DIMENSION = (200, 200)
RANDOM_SEED = "random" # "random" for random
TRACK_PER_ITERATION = True # track counts per iteration and plot later

# step size (ratio) = 2 * LHP / (num_steps - 1)
# --> num_steps = 1 + (2 * LHP / SSR)

if RANDOM_SEED is not "random":
    random.seed(RANDOM_SEED)

if TRACK_PER_ITERATION:
    counts_per_experiment = []

numcells = DIMENSION[0] * DIMENSION[1]
autom = SimpleCooperationAndAntagonism(CONFIG_FILE, dimension=DIMENSION)
params_initial = [autom.params[param] for param in PARAMS_TO_VARY]
counts_values = [] # counts per iteration
x_values = [] # parameter x-axis
for i in range(NUM_STEPS):
    print("Experiment step:", i + 1, "/", NUM_STEPS)
    if not HIGH_ONLY:
        p_new_values = [(1 - LOW_HIGH_PERCENT) * param_initial
                        + (2 * LOW_HIGH_PERCENT) * (i / (NUM_STEPS - 1)) * param_initial
                        for param_initial in params_initial]
    else:
        p_new_values = [param_initial
                        + LOW_HIGH_PERCENT * (i / (NUM_STEPS - 1)) * param_initial
                        for param_initial in params_initial]
    print(p_new_values)
    x_values.append(p_new_values[0]) # index 0 for plotting
    autom = SimpleCooperationAndAntagonism(CONFIG_FILE) # reset
    for j, param in enumerate(PARAMS_TO_VARY):
        autom.params[param] = p_new_values[j]

    cont = CAContainer(cellular_automaton=autom)
    if not TRACK_PER_ITERATION:
        cont.run(GENERATIONS, callback=lambda s, c : print("Step", s + 1))
        counts = cont.cellsCounter(countVarious, 6, normalize=False) # final cell counts
    else:
        counts_per_experiment.append([])
        for s in range(GENERATIONS):
            print("Step", s + 1)
            cont.run(1, callback=None)
            counts = cont.cellsCounter(countVarious, 6, normalize=False)
            counts_per_experiment[-1].append(counts)
    counts_values.append(counts)  # final cell counts
    print(counts)
    
    cont.exportImage(OUTPUT_PREFIX + str(i) + ".png", state_to_color_int, scaleup=4)
    cont.exportConcentrationFieldsImage(OUTPUT_PREFIX + str(i) + "_tc" + ".png", scaleup=4)

for i, c in enumerate(counts_values):
    if c[0] == 0: # prevent divbyzero for all cells dead
        counts_values[i][0] = 1
if TRACK_PER_ITERATION:
    for i in range(NUM_STEPS):
        for j, c in enumerate(counts_per_experiment[i]):
            if c[0] == 0:
                counts_per_experiment[i][j][0] = 1

fig_total, ax_total = plt.subplots() # scatter total density of cells
fig_ac, ax_ac = plt.subplots() # cooperator and antagonist ratios
fig_acprop, ax_acprop = plt.subplots() # cooperator and antagonist ratio relative to respective num.
fig_res, ax_res = plt.subplots() # resistance ratio

ax_total.plot(x_values, [c[0] / numcells for c in counts_values])
ax_total.set_xlabel("Parameter values: " + PARAMS_TO_VARY[0])
ax_total.set_ylabel("Cell density")
ax_total.set_title("Final cell density vs. parameter values")
ax_total.grid()

ax_ac.plot(x_values, [c[1] / c[0] for c in counts_values], color='r', label="Antagonist rate")
ax_ac.plot(x_values, [c[2] / c[0] for c in counts_values], color='g', label="Cooperator rate")
ax_ac.legend()
ax_ac.set_xlabel("Parameter values: " + PARAMS_TO_VARY[0])
ax_ac.set_ylabel("Mean production rate")
ax_ac.set_title("Toxin/siderophore production rates w.r.t. cell count vs. parameter values")
ax_ac.grid()

ax_res.plot(x_values, [c[3] / c[0] for c in counts_values])
ax_res.set_xlabel("Parameter values: " + PARAMS_TO_VARY[0])
ax_res.set_ylabel("Resistance ratio")
ax_res.set_title("Resistant cell ratio vs. parameter values")
ax_res.grid()

if TRACK_PER_ITERATION:
    fig_cc_per_exp, ax_cc_per_exp = plt.subplots() # cell counts over num generations per exp.
    for i in range(NUM_STEPS):
        label = str(round(x_values[i], 4))
        ax_cc_per_exp.plot([s for s in range(GENERATIONS)],
                           [c[0] / numcells for c in counts_per_experiment[i]],
                           label=label) # auto-color
    ax_cc_per_exp.legend()
    ax_cc_per_exp.set_xlabel("Generation")
    ax_cc_per_exp.set_ylabel("Cell density")
    ax_cc_per_exp.set_title("Cell density over time for different parameter values")
    ax_cc_per_exp.grid()

    fig_avg_tox_coop, ax_avg_tox_coop = plt.subplots() # cell counts over num generations per exp.
    for i in range(NUM_STEPS):
        label = str(round(x_values[i], 4))
        color = cm.get_cmap('viridis')(i / NUM_STEPS) # get color
        ax_avg_tox_coop.plot([s for s in range(GENERATIONS)],
                             [c[1] / c[0] for c in counts_per_experiment[i]],
                             label="Toxin - " + label,
                             marker='o',
                             markersize=3,
                             color=color) # toxin
        ax_avg_tox_coop.plot([s for s in range(GENERATIONS)],
                             [c[2] / c[0] for c in counts_per_experiment[i]],
                             label="Siderophore - " + label,
                             marker='1',
                             markersize=3,
                             color=color) # coop
    ax_avg_tox_coop.legend()
    ax_avg_tox_coop.set_xlabel("Generation")
    ax_avg_tox_coop.set_ylabel("Mean production rate")
    ax_avg_tox_coop.set_title("Toxin/siderophore production rates over time for different parameter values of "
                              + PARAMS_TO_VARY[0])
    ax_avg_tox_coop.grid()

for i, c in enumerate(counts_values):
    if c[1] == 0: # prevent divbyzero for all cells dead
        counts_values[i][1] = 1
    if c[2] == 0:
        counts_values[i][2] = 1

ax_acprop.plot(x_values, [int(c[4]) / c[1] for c in counts_values], color='r', label="Antagonist average production rate")
ax_acprop.plot(x_values, [int(c[5]) / c[2] for c in counts_values], color='g', label="Cooperator average production rate")
ax_acprop.legend()
ax_acprop.set_xlabel("Parameter values: " + PARAMS_TO_VARY[0])
ax_acprop.set_ylabel("Average production rate per phenotypic cell")
ax_acprop.set_title("Toxin/siderophore production rates averaged over phenotypic cells vs. parameter values")
ax_acprop.grid()
                          
plt.show()

