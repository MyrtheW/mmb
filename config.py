def loadConfig(filename):
    """Load and return a configuration in a configuration file."""
    params = {} # dictionary of parameters
    param_types = {} # dictionary of parameter types
    with open(filename) as f:
        for line in f.readlines():
            if len(line) > 1: # whitespace
                if line[0] != "#": # comment
                    l1 = line.split(":")
                    if len(l1) < 2:
                        raise ValueError("@loadConfig: error in line: " + line)
                    varname = l1[0].strip()
                    l2 = l1[1].split("=")
                    if len(l2) < 2:
                        raise ValueError("@loadConfig: error in line: " + line)
                    vartype = l2[0].strip()
                    value = l2[1].split("#")[0].strip() # comment after line allowed

                    conv_val = None
                    if vartype == "int":
                        conv_vartype = int
                        conv_val = int(value)
                    elif vartype == "float":
                        conv_vartype = float
                        conv_val = float(value)
                    elif vartype == "bool":
                        conv_vartype = bool
                        conv_val = bool(value)
                    elif vartype == "tuple":
                        conv_vartype = tuple
                        conv_val = tuple(value)
                    elif vartype == "list":
                        conv_vartype = list
                        conv_val = list(value)
                    elif vartype == "string" or vartype == "none" or vartype == "str":
                        conv_vartype = str
                        conv_val = value
                    else:
                        raise TypeError("@loadConfig: unsupported parameter type: " + vartype)

                    params[varname] = conv_val
                    param_types[varname] = conv_vartype
    return params, param_types

if __name__ == "__main__":
    print(loadConfig("./config.txt"))
                
                    
