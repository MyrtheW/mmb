import random
from cellular_automaton import CellularAutomaton, MooreNeighborhood, CAWindow, EdgeRule
import numpy as np
from scipy import signal
import config

ALIVE = 0
ANTAGONIST = 1
COOPERATOR = 2
AGE = 3
RESISTANCE = 4

class SimpleCooperationAndAntagonism(CellularAutomaton):
    """ Represents an automaton of antagonists, cooperators and normal individuals"""
    def __init__(self, config_file, dimension=[200, 200]):
        self.params, self.param_types = config.loadConfig(config_file)  # Loads the parameters
        super().__init__(dimension=dimension,
                         neighborhood=MooreNeighborhood(EdgeRule.FIRST_AND_LAST_CELL_OF_DIMENSION_ARE_NEIGHBORS))
        # Setting other attributes
        self.kernel_cooperator = self.gaussian_kernel(self.params['diffusion_length_siderophore'])
        self.kernel_antagonist = self.gaussian_kernel(self.params['diffusion_length_toxin'])
        self.antagonating_concentration = np.zeros(dimension)
        self.cooperating_concentration = np.zeros(dimension)

    def init_cell_state(self, __):
        alive = random.randint(0, 1)
        return [alive] \
               + [alive*random.choice([self.params['start_production_rate_antagonist'], 0])] \
               + [alive*random.choice([self.params['start_production_rate_cooperator'], 0])] \
               + [alive*int(random.uniform(0, self.params['age_threshold_death']))] \
               + [alive*random.randint(0, 1)]

    def evolve_rule(self, cell_state, coordinate, cell):
        new_cell_state = cell_state
        coordinates_empty_neighbors = self.__empty_neighbor_coordinates(cell)
        number_of_alive_neighbors = len(cell.neighbors) - len(coordinates_empty_neighbors) + 1
        if cell_state[ALIVE]:
            self.mutate(cell)
            fitness = - self.antagonating_concentration[coordinate] * self.params['effect_toxin'] * (1 - cell_state[RESISTANCE]) \
                      + self.cooperating_concentration[coordinate] * self.params['effect_siderophore'] \
                      - self.params['cost_resistance'] * cell_state[RESISTANCE] \
                      - self.params['cost_toxin_production'] * cell_state[ANTAGONIST] \
                      - self.params['cost_siderophore_production'] * cell_state[COOPERATOR] \
                      + self.params['nutrition'] / number_of_alive_neighbors

            if fitness < self.params['fitness_threshold_death'] or cell_state[AGE] > self.params['age_threshold_death']:
                new_cell_state = [0, 0, 0, 0, 0]
            elif fitness > self.params['fitness_threshold_replication'] and cell_state[AGE] > self.params['age_threshold_replication']:
                self.reproduce(cell, coordinates_empty_neighbors)
            else:
                new_cell_state[AGE] += 1
            self.antagonating_concentration[coordinate] += cell.state[ANTAGONIST]
            self.cooperating_concentration[coordinate] += cell.state[COOPERATOR]
        return new_cell_state

    def reproduce(self, cell, coordinates_empty_neighbors):
        if coordinates_empty_neighbors != []:
            child, child_coordinate = random.choice(coordinates_empty_neighbors)
            cell.state[AGE] = 0
            new_child_state = cell.state.copy()
            child.is_active = False
            self.evolve_cell(child, self._next_state[child_coordinate], new_child_state)

    def mutate(self, cell):
        alive_neighbours = self.__alive_neighbors(cell)
        for index in [ANTAGONIST, COOPERATOR, RESISTANCE]:
            if alive_neighbours:
                if not cell.state[index]:
                    if random.uniform(0, 1) < self.params['probability_plasmid']:
                        cell.state[index] = (random.choice(alive_neighbours)).state[index]
            elif random.uniform(0, 1) < self.params['probability_mutation']:
                if random.uniform(0,1) < self.params['probability_disfunction']:
                    cell.state[index] = 0
                elif index != RESISTANCE:
                    if cell.state[index]:
                        cell.state[index] = random.normalvariate(cell.state[index], self.params['deviation_through_mutation'])
                        if cell.state[index] < 0:
                            cell.state[index] = 0

    @staticmethod
    def __empty_neighbor_coordinates(cell):
        return [(neighbor, neighbor_coordinate) for neighbor, neighbor_coordinate
                                       in zip(cell.neighbors, cell.neighbor_coordinates)
                                       if not neighbor.state[ALIVE]]
    @staticmethod
    def __alive_neighbors(cell):
        return [neighbor for neighbor in cell.neighbors if neighbor.state[ALIVE]]

    @staticmethod
    def gaussian_kernel(sigma):
        kernel_size = round(sigma*6)
        if (kernel_size % 2) == 0:
            kernel_size += 1
        kernel_1d = signal.gaussian(kernel_size, std=sigma).reshape(kernel_size, 1)
        kernel = np.outer(kernel_1d, kernel_1d)
        return kernel / np.sum(kernel)

    def evolve_concentration(self, current_state):
        self.antagonating_concentration = signal.convolve2d(self.antagonating_concentration, self.kernel_antagonist, mode='same', boundary='symm')
        self.cooperating_concentration = signal.convolve2d(self.cooperating_concentration, self.kernel_cooperator, mode='same', boundary='symm')
        self.antagonating_concentration *= (1 - self.params['degredation_rate_toxin'])
        self.cooperating_concentration *= (1 - self.params['degredation_rate_siderophore'])

def state_to_color(current_state):
    """Red = Alive, Green = Antagonist, Blue = Cooperator"""
    return 255*(0.5 + 0.5*current_state[RESISTANCE]) if current_state[ANTAGONIST] and current_state[ALIVE] else 0, \
           255*(0.5 + 0.5*current_state[RESISTANCE]) if current_state[COOPERATOR] and current_state[ALIVE] else 0, \
           255*(0.5 + 0.5*current_state[RESISTANCE]) if current_state[ALIVE] and not current_state[ANTAGONIST] and not current_state[COOPERATOR] else 0


if __name__ == "__main__":
    CAWindow(cellular_automaton=SimpleCooperationAndAntagonism("./config.txt"),
             window_size=(1000, 830),
             state_to_color_cb=state_to_color).run()

