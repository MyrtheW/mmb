import random
from cellular_automaton import CellularAutomaton, MooreNeighborhood, CAWindow, EdgeRule
import numpy as np
from scipy import signal


#Indices:
ALIVE = 0
ANTAGONIST = 1
COOPERATOR = 2
AGE = 3

class SimpleCooperationAndAntagonism(CellularAutomaton):
    """ Represents an automaton of antagonists, cooperators and normal individuals"""

    def __init__(self):
        dimension = [200, 200]
        #Parameters: all defined positive
        self.cooperator_production_cost = 2
        self.antagonist_production_cost = 1
        self.cooperator_effect = 4
        self.antagonist_effect = 10
        self.mutate_probability = 0.1
        self.plasmid_transfer_probability = 0.1
        self.probability_disasterous_mutation = 0.1
        self.average_death_age = 40
        self.average_repl_age = 10
        self.deviation_age = 1
        self.diffusion_length_cooperator = 0.3
        self.diffusion_length_antagonist = 0.7
        self.nutrition = 10
        self.start_production_rate_cooperator = 1
        self.start_production_rate_antagonist = 1
        self.deviation_through_mutation = 0.1
        self.degredation_rate_cooperator = 0.4
        self.degredation_rate_antagonist = 0.4
        self.fitness_apoptosis_threshold = 2
        self.fitness_repl_threshold = 4

        super().__init__(dimension=dimension,
                         neighborhood=MooreNeighborhood(EdgeRule.FIRST_AND_LAST_CELL_OF_DIMENSION_ARE_NEIGHBORS))
        #seting other atributes
        self.kernel_cooperator = self.gaussian_kernel(self.diffusion_length_cooperator)
        self.kernel_antagonist = self.gaussian_kernel(self.diffusion_length_antagonist)
        self.antagonating_concentration = np.zeros(dimension)
        self.cooperating_concentration = np.zeros(dimension)

    def init_cell_state(self, __):
        return [random.randint(0, 1)] \
               + [random.choice([self.start_production_rate_antagonist, 0])] \
               + [random.choice([self.start_production_rate_cooperator, 0])] \
               + [0] # [Alive, Toxin production, Sidophore production, age]
    #we kunnen hier ook gewoon een dictionary van maken.

    def evolve_rule(self, cell_state, coordinate, cell):
        new_cell_state = cell_state
        coordinates_empty_neighbors = self.__empty_neighbor_coordinates(cell)
        number_of_alive_neighbors = len(cell.neighbors) - len(coordinates_empty_neighbors) + 1 #including oneself.
        if cell_state[ALIVE]:
            self.mutate(cell)
            fitness = - self.antagonating_concentration[coordinate] * self.antagonist_effect \
                      + self.cooperating_concentration[coordinate] * self.cooperator_effect \
                      - self.antagonist_production_cost * cell_state[ANTAGONIST] \
                      - self.cooperator_production_cost * cell_state[COOPERATOR] \
                      + self.nutrition/number_of_alive_neighbors

            if fitness < self.fitness_apoptosis_threshold or cell_state[AGE] > random.normalvariate(self.average_death_age, self.deviation_age):
                new_cell_state = [0, 0, 0, 0]
            elif fitness > self.fitness_repl_threshold and cell_state[AGE] > random.normalvariate(self.average_repl_age, self.deviation_age):
                self.reproduce(cell, coordinates_empty_neighbors)
            else:
                new_cell_state[AGE] += 1
            self.antagonating_concentration[coordinate] += cell.state[ANTAGONIST]
            self.cooperating_concentration[coordinate] += cell.state[COOPERATOR]
        return new_cell_state

    def reproduce(self, cell, coordinates_empty_neighbors):
        if coordinates_empty_neighbors != []:
            child, child_coordinate = random.choice(coordinates_empty_neighbors)
            cell.state[AGE] = 0
            new_child_state = cell.state.copy()
            child.is_active = False
            self.evolve_cell(child, self._next_state[child_coordinate], new_child_state)

    def mutate(self, cell):
        for index in [ANTAGONIST, COOPERATOR]:
            if not cell.state[index]:
                if random.uniform(0, 1) < self.plasmid_transfer_probability:
                    cell.state[index] = (random.choice(cell.neighbors)).state[index] #plasmid transfer
            else:
                if random.uniform(0, 1) < self.mutate_probability: #mutate
                    if random.uniform(0,1) < self.probability_disasterous_mutation:
                        cell.state[index] = 0
                    else:
                        cell.state[index] = random.normalvariate(cell.state[index], self.deviation_through_mutation)
        #mutate to death
        # #Willen we met of zonder fatale mutaties? Hier moet eigenlijk ook een andere probabaility voor zijn dan
        # if random.uniform(0, 1) < self.mutate_probability:
        #     cell.state = [0,0,0,0]

    @staticmethod
    def __empty_neighbor_coordinates(cell):
        return [(neighbor, neighbor_coordinate) for neighbor, neighbor_coordinate
                                       in zip(cell.neighbors, cell.neighbor_coordinates)
                                       if not neighbor.state[ALIVE]]

    @staticmethod
    def gaussian_kernel(sigma):
        kernel_size = round(sigma*6) #kernel size should round to odd(6*sigma)
        if (kernel_size % 2) == 0:
            kernel_size += 1 #of -1, als dat dichter in de buurt ligt bij sigma
        kernel_1d = signal.gaussian(kernel_size, std=sigma).reshape(kernel_size, 1)
        kernel = np.outer(kernel_1d, kernel_1d)
        return kernel / np.sum(kernel)

    def evolve_concentration(self, current_state):
        self.antagonating_concentration = signal.convolve2d(self.antagonating_concentration, self.kernel_antagonist, mode='same', boundary='symm')
        self.cooperating_concentration = signal.convolve2d(self.cooperating_concentration, self.kernel_cooperator, mode='same', boundary='symm')
        self.antagonating_concentration *= (1- self.degredation_rate_antagonist)
        self.cooperating_concentration *= (1- self.degredation_rate_cooperator)

def state_to_color(current_state):
    """Red = Alive, Green = Antagonist, Blue = Cooperator"""
    return 255 if current_state[ALIVE] and not current_state[ANTAGONIST] and not current_state[COOPERATOR] else 0, \
           255 if current_state[ANTAGONIST] and current_state[ALIVE] else 0, \
           255 if current_state[COOPERATOR] and current_state[ALIVE] else 0


if __name__ == "__main__":
    CAWindow(cellular_automaton= SimpleCooperationAndAntagonism(),
             window_size=(1000, 830),
             state_to_color_cb=state_to_color).run()

