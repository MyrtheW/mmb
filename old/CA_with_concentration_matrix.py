import random
from cellular_automaton import CellularAutomaton, MooreNeighborhood, CAWindow, EdgeRule
import numpy as np
from scipy import signal


#Indices:
ALIVE = 0
ANTAGONIST = 1
COOPERATOR = 2

class SimpleCooperationAndAntagonism(CellularAutomaton):
    """ Represents an automaton of antagonists, cooperators and normal individuals"""

    def __init__(self):
        dimension = [200, 200]
        super().__init__(dimension=dimension,
                         neighborhood=MooreNeighborhood(EdgeRule.FIRST_AND_LAST_CELL_OF_DIMENSION_ARE_NEIGHBORS))
        self.antagonating_concentration = np.zeros(dimension)
        self.cooperating_concentration = np.zeros(dimension)
        self.kernel = np.array([
                    [1, 2, 1],
                    [2, 2, 2],
                    [1, 2, 1]]) / 14 #maak later gaussian kernels op basis van diffusie lengtes.

        #Parameters: all defined positive
        self.cooperator_production_cost = 6
        self.antagonist_production_cost = 5
        self.cooperator_effect = 3
        self.antagonist_effect = 4
        self.mutate_probability = 0.1

    def init_cell_state(self, __):
        return [random.randint(0, 2) for _ in range(3)] # [Alive, Toxin production, Sidophore production]
    #we kunnen hier ook gewoon een dictionary van maken.

    def evolve_rule(self, cell_state, coordinate, cell):
        new_cell_state = cell_state

        if cell_state[ALIVE]:
            self.mutate(cell)
            fitness = - self.antagonating_concentration[coordinate] * self.antagonist_effect \
                      + self.cooperating_concentration[coordinate] * self.cooperator_effect \
                      - self.antagonist_production_cost * cell_state[ANTAGONIST] \
                      - self.cooperator_production_cost * cell_state[COOPERATOR]
            if fitness < -10:
                #Ik weet niet wat een goede threshold is voor uitsterven.
                #Eigenlijk wil je dat de fitness altijd in een bepaalde range ligt, waardoor je kan gebruiken om een kans te berekenen dat iets uitsterft.
                new_cell_state = [0, 0, 0]
            elif fitness > 0:
                #andere threshold voor reproductie. Geen idee wat een goed threshold is en of we hier ook een kans willen gebruiken.
                self.reproduce(cell)
        return new_cell_state

    def reproduce(self, cell):
        coordinates_empty_neighbors = self.__empty_neighbor_coordinates(cell)
        if coordinates_empty_neighbors != []:
            child, child_coordinate = random.choice(coordinates_empty_neighbors)
            self.evolve_cell(child, child, cell.state) #updates the current_state, weet niet of dit ook de next_state dictionary goed update later.
            #anders moet dit werken:
            child.is_active = False
            self.evolve_cell(child, self._next_state[child_coordinate], cell.state)

    def mutate(self, cell):
        """
        parameters die gemuteerd zouden kunnen worden:
            - hoeveelheid van sidofoor/toxine productie (door niet binaire waardes te gebruiken, maar een range van 0,1)
            - sidofoor/toxine diffusie lengte
            - kosten voor sidofoor/toxine productie
            - effect van sidofoor/toxine
        Als we deze laatste 3 waarden ook willen laten muteren, moeten we het ook storen in de cell state lijst, ipv als class parameter,
        """
        if random.uniform(0,1) < self.mutate_probability:
            cell.state[ANTAGONIST] = (1 - cell.state[ANTAGONIST])
        if random.uniform(0,1) < self.mutate_probability:
            cell.state[COOPERATOR] = (1 - cell.state[COOPERATOR])

    @staticmethod
    def __empty_neighbor_coordinates(cell):
        return [(neighbor, neighbor_coordinate) for neighbor, neighbor_coordinate
                                       in zip(cell.neighbors, cell.neighbor_coordinates)
                                       if not neighbor.state[ALIVE]]

    def evolve_concentration(self, current_state):
        self.antagonating_concentration = signal.convolve2d(self.antagonating_concentration, self.kernel, mode='same')
        self.cooperating_concentration = signal.convolve2d(self.cooperating_concentration, self.kernel, mode='same')
        for coordinate, cell in current_state.items():
            #voor elke coordinaat een soort evaporation.
            if cell.state[ALIVE]: #aka, als levend (als een cel dood is, zou deen die sidofoor en toxin prodcutie eigenlijk uit moeten staan.
                self.antagonating_concentration[coordinate] += cell.state[ANTAGONIST]
                #telt bij elke cell met antagonator er 1 bij op.
                self.cooperating_concentration[coordinate] += cell.state[COOPERATOR]
                #kernel erop loslaten

def state_to_color(current_state):
    """Red = Alive, Green = Antagonist, Blue = Cooperator"""
    return 255 if current_state[ALIVE] and not current_state[ANTAGONIST] and not current_state[COOPERATOR] else 0, \
           255 if current_state[ANTAGONIST] and current_state[ALIVE] else 0, \
           255 if current_state[COOPERATOR] and current_state[ALIVE] else 0


if __name__ == "__main__":
    CAWindow(cellular_automaton= SimpleCooperationAndAntagonism(),
             window_size=(1000, 830),
             state_to_color_cb=state_to_color).run()

