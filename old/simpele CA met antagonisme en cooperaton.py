import random
from cellular_automaton import CellularAutomaton, MooreNeighborhood, CAWindow, EdgeRule

COOPERATOR = [1]
NORMAL = [2]
ANTAGONIST = [3]
EMPTY = [0]

class SimpleCooperationAndAntagonism(CellularAutomaton):
    """ Represents an automaton of antagonists, cooperators and normal individuals"""

    def __init__(self, cooperator_cost, antagonist_gain):
        super().__init__(dimension=[200, 200],
                         neighborhood=MooreNeighborhood(EdgeRule.FIRST_AND_LAST_CELL_OF_DIMENSION_ARE_NEIGHBORS))
        self.c = cooperator_cost
        self.a = antagonist_gain

    def init_cell_state(self, __):
        return [random.randint(1, 4)]

    def evolve_rule(self, cell_state, coordinate, cell):
        last_cell_state = cell_state
        neighbors_last_states = cell.neighbors
        new_cell_state = last_cell_state
        cooperating_neighbours, antagonating_neighbours = self.__count_neighbours(neighbors_last_states)
        total_neighbours = len(neighbors_last_states)
        normal_neighbours = total_neighbours - cooperating_neighbours - antagonating_neighbours

        if last_cell_state == COOPERATOR:
            energy = - self.c * total_neighbours - self.a * antagonating_neighbours + self.c * cooperating_neighbours
        elif last_cell_state == ANTAGONIST:
            energy = self.a * total_neighbours - self.a * antagonating_neighbours + self.c * cooperating_neighbours
        else: #if last_cell_state == NORMAL:
            energy = - self.a * antagonating_neighbours + self.c * cooperating_neighbours

        if energy < 0:
            if cooperating_neighbours > antagonating_neighbours > normal_neighbours:
                new_cell_state = COOPERATOR
            elif cooperating_neighbours == antagonating_neighbours > normal_neighbours:
                new_cell_state = random.choice([COOPERATOR, ANTAGONIST])
            elif antagonating_neighbours > cooperating_neighbours > normal_neighbours:
                new_cell_state = ANTAGONIST
            else:
                new_cell_state = NORMAL  # EMPTY
        return new_cell_state

    @staticmethod
    def __count_neighbours(neighbours):
        cooperating_neighbours, antagonating_neighbours = 0, 0
        for n in neighbours:
            if n == COOPERATOR:
                cooperating_neighbours += 1
            elif n == ANTAGONIST:
                antagonating_neighbours += 1

        return cooperating_neighbours, antagonating_neighbours


def state_to_color(current_state):
    return 255 if current_state[0] == 1 else 0, \
           255 if current_state[0] == 2 else 0, \
           255 if current_state[0] == 3 else 0


if __name__ == "__main__":
    CAWindow(cellular_automaton= SimpleCooperationAndAntagonism(6, 20),
             window_size=(1000, 830),
             state_to_color_cb=state_to_color).run()

